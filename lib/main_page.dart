import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lapor_premanisme/item_card.dart';

class MainPage extends StatelessWidget {
  final TextEditingController judulLaporanController = TextEditingController();
  final TextEditingController deskripsiLaporanController =
      TextEditingController();
  final TextEditingController errorTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference laporan = firestore.collection('laporan');

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(children: [
            Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                child: Image.network(
                    'https://www.linkpicture.com/q/logo-lp-1.png')),
            Text(
              'Form Pelaporan',
              style: TextStyle(
                color: Colors.red[800],
              ),
            ),
          ]),
        ),
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            ListView(
              children: [
                //// VIEW DATA HERE
                FutureBuilder<QuerySnapshot>(
                  future: laporan.get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasData) {
                      final List<DocumentSnapshot> documents =
                          snapshot.data.docs;
                      return Column(
                        children: documents
                            .map((e) => ItemCard(
                                e.data()['judul'], e.data()['deskripsi']))
                            .toList(),
                      );
                    } else {
                      return Text('Loading');
                    }
                  },
                ),
                SizedBox(
                  height: 100,
                )
              ],
            ),
            Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(-5, 0),
                        blurRadius: 15,
                        spreadRadius: 3)
                  ]),
                  width: double.infinity,
                  height: 1000,
                  child: ListView(
                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 120,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextField(
                              style: GoogleFonts.poppins(),
                              controller: judulLaporanController,
                              decoration:
                                  InputDecoration(labelText: "Judul Laporan"),
                            ),
                            TextField(
                              style: GoogleFonts.poppins(),
                              controller: deskripsiLaporanController,
                              decoration: InputDecoration(
                                  labelText: "Deskripsi Laporan"),
                              keyboardType: TextInputType.multiline,
                              maxLines: 4,
                            ),
                          ],
                        ),
                      ),
                      Container(
                          height: 300,
                          width: 300,
                          padding: const EdgeInsets.fromLTRB(0, 25, 0, 15),
                          child: Column(children: [
                            RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.red[800],
                                child: Text(
                                  'LAPOR',
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  if (judulLaporanController.text != "" ||
                                      deskripsiLaporanController.text != "") {
                                    laporan.add({
                                      'judul': judulLaporanController.text,
                                      'deskripsi':
                                          deskripsiLaporanController.text,
                                      'status_laporan': 0,
                                    });

                                    judulLaporanController.text = "";
                                    deskripsiLaporanController.text = "";
                                  }
                                }),
                            RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.white,
                                child: Text(
                                  'KEMBALI',
                                  style: GoogleFonts.poppins(
                                      color: Colors.red[800],
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {}),
                          ])),
                    ],
                  ),
                )),
          ],
        ));
  }
}
