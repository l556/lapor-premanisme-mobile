import 'package:firebase_auth/firebase_auth.dart';
import 'package:lapor_premanisme/database-manager/laporan_db.dart';

class AuthenticationService {
  final FirebaseAuth _firebaseAuth;

  AuthenticationService(this._firebaseAuth);

  Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<String> signIn({String email, String password}) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      return "Signed In";
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  Future<String> signUp({
    String email,
    String password,
    String nik,
    String nama_lengkap,
    String nomor_telepon,
    String tanggal_lahir,
  }) async {
    try {
      await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((creds) => {
                print(creds),
                LaporanDB().setUser(creds.user.uid, nik, nama_lengkap,
                    nomor_telepon, tanggal_lahir)
              });
      return "Signed Up";
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }
}
