import 'package:cloud_firestore/cloud_firestore.dart';

class LaporanDB {
  final CollectionReference laporanList =
      FirebaseFirestore.instance.collection('laporan_prod');

  final CollectionReference profilList =
      FirebaseFirestore.instance.collection("profil");

  Future<void> createLaporan(String judul, String deskripsi, String id, int noLaporan, String lokasi) async {
    DateTime now = new DateTime.now();
    return laporanList
        .add({
      'id': id, 'judul': judul,
      'deskripsi': deskripsi, 'status_laporan': 0,
      'tanggal_pelaporan': now,
      'lokasi': lokasi,
      'is_sedang_diverifikasi': false,
      'is_sedang_diproses': false,
      'is_sudah_selesai': false,
      'tanggal_diverifikasi': null,
      'tanggal_diproses': null,
      'tanggal_selesai': null,
      'deskripsi_verifikasi': null,
      'deskripsi_diproses':null,
      'deskripsi_selesai':null,
      'no_laporan': noLaporan
        });
  }

  Future<void> setUser(
    String uid,
    String nik,
    String nama_lengkap,
    String nomor_telepon,
    String tanggal_lahir,
  ) async {
    return await profilList.doc(uid).set({
      'nik': nik,
      'nama_lengkap': nama_lengkap,
      'nomor_telepon': nomor_telepon,
      "tanggal_lahir": tanggal_lahir,
    });
  }

  Future updateUserList(
      String judul, String deskripsi, String status, String uid) async {
    return await laporanList.doc(uid).update(
        {'judul': judul, 'deskripsi': deskripsi, 'status_laporan': status});
  }

  Future getLaporanList() async {
    List itemsList = [];

    try {
      await laporanList.get().then((querySnapshot) {
        querySnapshot.docs.forEach((element) {
          itemsList.add(element.data());
        });
      });
      // print(itemsList);
      return itemsList;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future getUserProfiles() async {
    List itemsList = [];

    try {
      await profilList.get().then((querySnapshot) {
        querySnapshot.docs.forEach((element) {
          itemsList.add(element);
        });
      });
      return itemsList;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
