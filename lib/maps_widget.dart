import 'package:flutter/material.dart';
import 'package:place_picker/place_picker.dart';

class MyMaps extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PickerDemoState();
}

class PickerDemoState extends State<MyMaps> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Picker Example')),
      body: Center(
        child: FlatButton(
          child: Text("Pilih Lokasi"),
          onPressed: () {
            showPlacePicker();
          },
        ),
      ),
    );
  }

  void showPlacePicker() async {
    LocationResult result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            PlacePicker("AIzaSyAHoJvEQFN7x2iWWalFOGLS6Lxk0u8Ybts")));

    // Handle the result in your way
    print(result);
  }
}
