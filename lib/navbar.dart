import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:lapor_premanisme/screens/laporan-saya/detail_laporan_saya.dart';
import 'screens/laporan-saya/laporan_saya.dart';
import 'screens/beranda/beranda.dart';
import 'screens/form-laporan/form_laporan.dart';
import 'package:lapor_premanisme/screens/profil-saya/profil_saya_read.dart';
import 'screens/laporan-saya/laporan_saya.dart';
import 'screens/pengaturan/pengaturan.dart';

class NavBar extends StatefulWidget {
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  int _selectedNavbar = 0;

  List<Widget> _widgetOptions = <Widget>[
    Beranda(),
    LaporanSaya(),
    ProfilSayaRead(),
    Pengaturan(),
  ];

  void _changeSelectedNavBar(int index) {
    setState(() {
      _selectedNavbar = index;
    });
  }

  void _navigateToNextScreen(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => FormLaporan()))
        .then((value) {
      if (value != null) {
        setState(() {
          // print("${value['judul']} VALUE 0");
          _selectedNavbar = 0;
          toDetailLaporan(value);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedNavbar),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.brown[800],
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Beranda',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            label: 'Laporan Saya',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profil Saya',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Pengaturan',
          ),
        ],
        currentIndex: _selectedNavbar,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
        onTap: _changeSelectedNavBar,
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('LAPOR'),
        tooltip: 'Tambah Laporan',
        icon: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: Colors.red[800],
        onPressed: () {
          _navigateToNextScreen(context);
        },
      ),
    );
  }

  void toDetailLaporan(value) async {
    // print("VALUE ${value[0]}");
    var res = await value.get();
    var user = FirebaseAuth.instance.currentUser;
    var urlFirestorage = 'foto-pendukung-laporan/${user.uid}/${res['no_laporan']}';
    final ref = await FirebaseStorage
        .instance.ref().child(urlFirestorage);
    var url = null;
    try {
      url = await ref.getDownloadURL().timeout(const Duration(seconds: 10));
    }
    catch (e) {
    }
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => DetailLaporan(res, url)))
    .then((value) {
      setState(() {
        _selectedNavbar = 1;
      });
    });
  }
}
