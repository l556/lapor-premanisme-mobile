import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:lapor_premanisme/services/authentication_service.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:nik_validator/nik_validator.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool _signIn = false;
  bool _beginningScreen = true;

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController nomorTeleponController = TextEditingController();
  final TextEditingController nikController = TextEditingController();
  final TextEditingController namaLengkapController = TextEditingController();
  TextEditingController tanggalLahirController = TextEditingController();

  bool _passwordVisible = false;
  bool _validateEmail = false;
  bool _validateInvalidEmail = false;
  bool _validatePassword = false;
  bool _validateNomorTelepon = false;
  bool _validateMinNomorTelepon = false;
  bool _validateNik = false;
  bool _validateNamaLengkap = false;
  bool _validateTanggalLahir = false;

  DateTime selectedDate = DateTime.now();
  NIKModel nikResult;
  bool _isNikValid = true;

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1921),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        tanggalLahirController.text = DateFormat('yMd').format(picked);
      });
  }

  void inputValidator() {
    setState(() {
      (emailController.text == "")
          ? _validateEmail = true
          : _validateEmail = false;

      (passwordController.text == "")
          ? _validatePassword = true
          : _validatePassword = false;

      (nikController.text == "") ? _validateNik = true : _validateNik = false;
      (namaLengkapController.text == "")
          ? _validateNamaLengkap = true
          : _validateNamaLengkap = false;

      (nomorTeleponController.text == "")
          ? _validateNomorTelepon = true
          : _validateNomorTelepon = false;

      (tanggalLahirController.text == "")
          ? _validateTanggalLahir = true
          : _validateTanggalLahir = false;
    });
  }

  ///Validate NIK informations
  void validate() async {
    if (nikController.text.isNotEmpty) {
      NIKModel result =
          await NIKValidator.instance.parse(nik: nikController.text);

      bool minLengthValid = nomorTeleponController.text.length >= 7;
      bool emailValid = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(emailController.text);

      if (result.valid && minLengthValid && emailValid) {
        setState(() {
          nikResult = result;
          _isNikValid = true;
          _validateMinNomorTelepon = false;
          _validateInvalidEmail = false;

          context.read<AuthenticationService>().signUp(
              email: emailController.text.trim(),
              password: passwordController.text.trim(),
              nik: nikController.text.trim(),
              nama_lengkap: namaLengkapController.text,
              nomor_telepon: nomorTeleponController.text.trim(),
              tanggal_lahir: tanggalLahirController.text);
        });
      } else {
        setState(() {
          if (!result.valid) {
            _isNikValid = false;
          } else {
            _isNikValid = true;
          }

          _validateNik = false;

          if (!emailValid) {
            _validateInvalidEmail = true;
          } else {
            _validateInvalidEmail = false;
          }
          _validateEmail = false;

          if (!minLengthValid) {
            _validateMinNomorTelepon = true;
          } else {
            _validateMinNomorTelepon = false;
          }
          _validateNomorTelepon = false;

          inputValidator();
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
    _signIn = false;
  }

  @override
  Widget build(BuildContext context) {
    return (_beginningScreen)
        ? Scaffold(
            // resizeToAvoidBottomInset: false,
            body: Center(
                child: Container(
                    margin: EdgeInsets.only(
                        left: 30, top: 10, right: 30, bottom: 10),
                    child: Wrap(children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(children: [
                            Image.network(
                                'https://www.linkpicture.com/q/logo-lp-1.png'),
                            SizedBox(height: 20),
                            Text(
                              'Lapor Premanisme',
                              style: new TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 22.0,
                                color: Colors.red[800],
                              ),
                            ),
                          ]),
                          SizedBox(height: 50),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.white,
                                child: Text(
                                  'SIGN UP',
                                  style: GoogleFonts.poppins(
                                      color: Colors.red[800],
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  setState(() {
                                    _beginningScreen = false;
                                  });
                                },
                              ),
                              SizedBox(width: 25),
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.red[800],
                                child: Text(
                                  'LOGIN',
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  setState(() {
                                    _beginningScreen = false;
                                    _signIn = true;
                                  });
                                },
                                // child: Text("Sign in"),
                              ),
                            ],
                          )
                        ],
                      ),
                    ]))),
          )
        : (_signIn)
            ? Scaffold(
                // resizeToAvoidBottomInset: false,
                body: Center(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 30, top: 10, right: 30, bottom: 10),
                        child: Wrap(children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(children: [
                                Image.network(
                                    'https://www.linkpicture.com/q/logo-lp-1.png'),
                                SizedBox(height: 10),
                                Text(
                                  'Lapor Premanisme',
                                  style: new TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20.0,
                                    color: Colors.red[800],
                                  ),
                                ),
                              ]),
                              SizedBox(height: 25),
                              TextField(
                                controller: emailController,
                                decoration: InputDecoration(
                                  labelText: "Email",
                                  labelStyle: TextStyle(color: Colors.black54),
                                  border: new OutlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.black)),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.black54, width: 1.5),
                                    // borderRadius: BorderRadius.circular(25.0),
                                  ),
                                  // border: new OutlineInputBorder(
                                  //     borderSide: new BorderSide(color: Colors.black)),
                                ),
                              ),
                              SizedBox(height: 25),
                              TextField(
                                controller: passwordController,
                                obscureText: !_passwordVisible,
                                decoration: InputDecoration(
                                  labelText: "Password",
                                  labelStyle: TextStyle(color: Colors.black54),
                                  border: new OutlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.black)),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.black54, width: 1.5),
                                    // borderRadius: BorderRadius.circular(25.0),
                                  ),
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      _passwordVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.black54,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _passwordVisible = !_passwordVisible;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(height: 25),
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.red[800],
                                child: Text(
                                  'LOGIN',
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  context.read<AuthenticationService>().signIn(
                                        email: emailController.text.trim(),
                                        password:
                                            passwordController.text.trim(),
                                      );
                                },
                                // child: Text("Sign in"),
                              ),
                              SizedBox(height: 15),
                              RichText(
                                  text: TextSpan(
                                      text: 'Tidak punya akun?',
                                      style: TextStyle(
                                          color: Colors.black87, fontSize: 16),
                                      children: <TextSpan>[
                                    TextSpan(
                                        text: ' Daftar',
                                        style: TextStyle(
                                            color: Colors.redAccent,
                                            fontSize: 16),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            setState(() {
                                              _signIn = false;
                                            });
                                            emailController.text = "";
                                            passwordController.text = "";
                                          })
                                  ])),
                            ],
                          ),
                        ]))),
              )
            : Scaffold(
                // resizeToAvoidBottomInset: false,
                body: Center(
                    child: SingleChildScrollView(
                        child: (Container(
                            margin: EdgeInsets.only(
                                left: 30, top: 10, right: 30, bottom: 10),
                            child: Wrap(children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(children: [
                                    Image.network(
                                        'https://www.linkpicture.com/q/logo-lp-1.png'),
                                    SizedBox(height: 20),
                                    Text(
                                      'Lapor Premanisme',
                                      style: new TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20.0,
                                        color: Colors.red[800],
                                      ),
                                    ),
                                    SizedBox(height: 25),
                                    Text(
                                      'Pendaftaran Akun',
                                      style: new TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 25.0,
                                        color: Colors.red[800],
                                      ),
                                    ),
                                  ]),
                                  SizedBox(height: 20),
                                  TextField(
                                    controller: emailController,
                                    decoration: InputDecoration(
                                      labelText: "Email",
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                      border: new OutlineInputBorder(
                                          borderSide: new BorderSide(
                                              color: Colors.black)),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black54, width: 1.5),
                                      ),
                                      errorText: _validateEmail
                                          ? 'Mohon diisi'
                                          : _validateInvalidEmail
                                              ? 'Email tidak valid'
                                              : null,
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  TextField(
                                    controller: passwordController,
                                    obscureText: !_passwordVisible,
                                    decoration: InputDecoration(
                                      labelText: "Password",
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                      border: new OutlineInputBorder(
                                          borderSide: new BorderSide(
                                              color: Colors.black)),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black54, width: 1.5),
                                      ),
                                      errorText: _validatePassword
                                          ? 'Mohon diisi'
                                          : null,
                                      suffixIcon: IconButton(
                                        icon: Icon(
                                          _passwordVisible
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          color: Colors.black54,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _passwordVisible =
                                                !_passwordVisible;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  TextField(
                                    controller: namaLengkapController,
                                    decoration: InputDecoration(
                                      labelText: "Nama Lengkap",
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                      border: new OutlineInputBorder(
                                          borderSide: new BorderSide(
                                              color: Colors.black)),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black54, width: 1.5),
                                      ),
                                      errorText: _validateNamaLengkap
                                          ? 'Mohon diisi'
                                          : null,
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  TextField(
                                    controller: nikController,
                                    keyboardType: TextInputType.number,
                                    maxLength: 16,
                                    decoration: InputDecoration(
                                      labelText: "NIK",
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                      border: new OutlineInputBorder(
                                          borderSide: new BorderSide(
                                              color: Colors.black)),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black54, width: 1.5),
                                      ),
                                      counterStyle: TextStyle(
                                        height: double.minPositive,
                                      ),
                                      counterText: "",
                                      errorText: _validateNik
                                          ? 'Mohon diisi'
                                          : (_isNikValid
                                              ? null
                                              : 'NIK tidak valid'),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  TextField(
                                    controller: nomorTeleponController,
                                    keyboardType: TextInputType.phone,
                                    maxLength: 13,
                                    decoration: InputDecoration(
                                      labelText: "Nomor Telepon",
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                      border: new OutlineInputBorder(
                                          borderSide: new BorderSide(
                                              color: Colors.black)),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black54, width: 1.5),
                                      ),
                                      counterStyle: TextStyle(
                                        height: double.minPositive,
                                      ),
                                      counterText: "",
                                      errorText: _validateNomorTelepon
                                          ? 'Mohon diisi'
                                          : _validateMinNomorTelepon
                                              ? 'Mohon diisi dengan nomor yang valid'
                                              : null,
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  TextField(
                                    controller: tanggalLahirController,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: "Tanggal Lahir",
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                      border: new OutlineInputBorder(
                                          borderSide: new BorderSide(
                                              color: Colors.black)),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black54, width: 1.5),
                                      ),
                                      errorText: _validateTanggalLahir
                                          ? 'Mohon diisi'
                                          : null,
                                    ),
                                    onTap: () => {
                                      // FocusScope.of(context).unfocus(),
                                      _selectDate(context)
                                    },
                                  ),
                                  SizedBox(height: 20),
                                  RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(4)),
                                    color: Colors.red[800],
                                    child: Text(
                                      'DAFTAR',
                                      style: GoogleFonts.poppins(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    onPressed: () {
                                      if (emailController.text != "" &&
                                          passwordController.text != "" &&
                                          nikController.text != "" &&
                                          namaLengkapController.text != "" &&
                                          nomorTeleponController.text != "" &&
                                          tanggalLahirController.text != "") {
                                        validate();
                                      } else {
                                        validate();
                                        inputValidator();
                                      }
                                    },
                                    // child: Text("Sign in"),
                                  ),
                                  SizedBox(height: 15),
                                  RichText(
                                      text: TextSpan(
                                          text: 'Sudah punya akun?',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 16),
                                          children: <TextSpan>[
                                        TextSpan(
                                            text: ' Masuk',
                                            style: TextStyle(
                                                color: Colors.redAccent,
                                                fontSize: 16),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                setState(() {
                                                  _signIn = true;
                                                });
                                                emailController.text = "";
                                                passwordController.text = "";
                                                nikController.text = "";
                                                namaLengkapController.text = "";
                                                tanggalLahirController.text =
                                                    "";
                                                nomorTeleponController.text =
                                                    "";
                                              })
                                      ])),
                                ],
                              ),
                            ]))))),
              );
  }
}
