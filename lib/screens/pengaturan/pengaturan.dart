import 'package:flutter/material.dart';
import 'package:lapor_premanisme/services/authentication_service.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';

class Pengaturan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(children: [
          Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
              child:
                  Image.network('https://www.linkpicture.com/q/logo-lp-1.png')),
          Text(
            'Pengaturan',
            style: TextStyle(
              color: Colors.red[800],
            ),
          ),
        ]),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Text("Pengaturan"),
            RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)),
              color: Colors.red[800],
              child: Text(
                'SIGN OUT',
                style: GoogleFonts.poppins(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                context.read<AuthenticationService>().signOut();
              },
            ),
          ],
        ),
      ),
    );

    // return Scaffold(
    //   body: Center(
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       children: [
    //         Text("Pengaturan"),
    //         ElevatedButton(
    //           onPressed: () {
    //             context.read<AuthenticationService>().signOut();
    //           },
    //           child: Text("Sign out"),
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }
}
