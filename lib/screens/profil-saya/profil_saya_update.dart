import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lapor_premanisme/screens/profil-saya/profil_saya_read.dart';
import 'package:path_provider/path_provider.dart';

class ProfilSayaUpdate extends StatefulWidget {
  String id;
  ProfilState readDataProfil;

  ProfilSayaUpdate(readDataProfil) {
    this.id = readDataProfil.user.uid;
    this.readDataProfil = readDataProfil;
  }

  @override
  _ProfilUpdateState createState() => _ProfilUpdateState(id, readDataProfil);
}

class _ProfilUpdateState extends State<ProfilSayaUpdate> {
  final TextEditingController namaLengkapController = TextEditingController();
  final TextEditingController nikController = TextEditingController();


  String id;
  ProfilState readDataProfil;
  String url;
  var image = null;

  var nomorTeleponController = TextEditingController();

  DateTime selectedDate = DateTime.now();

  var tanggalLahirController = TextEditingController();

  bool _validateNamaLengkap = false;
  bool _validateMinNomorTelepon = false;
  bool _validateNomorTelepon = false;

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1921),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        tanggalLahirHolder = DateFormat('yMd').format(picked);
      });
  }

  _ProfilUpdateState(String id, ProfilState readDataProfil) {
    this.id = id;
    this.readDataProfil = readDataProfil;
    this.url = readDataProfil.url;
    image = NetworkImage(url);
    tanggalLahirHolder = readDataProfil.tanggalLahir;
    namaLengkapController.text = readDataProfil.namaLengkap;
    nikController.text = readDataProfil.nik;
    nomorTeleponController.text = readDataProfil.nomorTelepon;
  }

  PickedFile imageFile = null;

  Future<File> getImageFileFromAssets(String path) async {
    final byteData = await rootBundle.load('assets/$path');

    final file = File('${(await getTemporaryDirectory()).path}/$path');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return file;
  }

  Future uploadFile(BuildContext context) async {
    File image;
    if (imageFile == null) {
      image = await getImageFileFromAssets("images/user.png");
    } else {
      image = File(imageFile.path);
    }
    UploadTask uploadTask;

    Reference ref = FirebaseStorage.instance.ref().child('foto-profil/${id}');

    uploadTask = ref.putFile(image);

    return Future.value(uploadTask);
  }

  var tanggalLahirHolder = null;

  @override
  Widget build(BuildContext context) {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference profil = firestore.collection('profil');

    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: 100,
                ),
              ],
            ),
            Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration:
                  BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(-5, 0),
                        blurRadius: 15,
                        spreadRadius: 3)
                  ]),
                  width: double.infinity,
                  height: 1000,
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 80,
                      ),
                      Column(children: [
                        Image.network(
                            'https://www.linkpicture.com/q/logo-lp-1.png'),
                        SizedBox(height: 20),
                        Text(
                          'PROFIL',
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 22.0,
                            color: Colors.red[800],
                          ),
                        ),
                      ]),
                      SizedBox(
                        height: 15,
                      ),
                      Center(
                        child: Stack(children: <Widget>[
                          CircleAvatar(
                            radius: 80.0,
                            backgroundImage: image,
                          ),
                          Positioned(
                            bottom: 0.0,
                            right: 0.0,
                            child: InkWell(
                                onTap: () {
                                  _showChoiceDialog(context);
                                },
                                child: RawMaterialButton(
                                  fillColor: Colors.white,
                                  shape: CircleBorder(),
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.blue,
                                    size: 28.0,
                                  ),
                                ),
                            ),
                          ),
                        ]),
                      ),
                      Container(
                          margin: EdgeInsets.only(
                              left: 30, top: 10, right: 30, bottom: 10),
                          child: Wrap(children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(height: 25),
                                TextField(
                                  controller: namaLengkapController,
                                  decoration: InputDecoration(
                                    labelText: "Nama Lengkap",
                                    labelStyle:
                                    TextStyle(color: Colors.black54),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.black)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.black54,
                                          width: 1.5),
                                    ),
                                    errorText: _validateNamaLengkap
                                        ? 'Mohon diisi'
                                        : null,
                                  ),
                                ),
                                SizedBox(height: 20),
                                TextField(
                                  controller: nomorTeleponController,
                                  keyboardType: TextInputType.phone,
                                  maxLength: 16,
                                  decoration: InputDecoration(
                                    labelText: "Nomor Telepon",
                                    labelStyle:
                                    TextStyle(color: Colors.black54),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.black)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.black54,
                                          width: 1.5),
                                    ),
                                    counterStyle: TextStyle(
                                      height: double.minPositive,
                                    ),
                                    counterText: "",
                                    errorText: _validateNomorTelepon
                                        ? 'Mohon diisi': _validateMinNomorTelepon ?
                                        "Mohon diisi dengan nomor yang valid" : null,
                                  ),
                                ),
                                SizedBox(height: 20),
                                TextField(
                                  controller: tanggalLahirController
                                    ..text = tanggalLahirHolder,
                                  readOnly: true,
                                  decoration: InputDecoration(
                                    labelText: "Tanggal Lahir",
                                    labelStyle:
                                    TextStyle(color: Colors.black54),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Colors.black)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.black54,
                                          width: 1.5),
                                    ),
                                  ),
                                  onTap: () =>
                                  {
                                    _selectDate(context)
                                  },
                                ),
                              ],
                            ),
                          ])),
                      Container(
                          padding: const EdgeInsets.fromLTRB(0, 25, 0, 15),
                          child: Column(children: [
                            RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.red[800],
                                child: Text(
                                  'Simpan',
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  if (namaLengkapController.text != "" &&
                                      nomorTeleponController.text != "" &&
                                      nomorTeleponController.text.length >= 7
                                  ) {
                                    uploadFile(context);
                                    profil.doc(id).update({
                                      "nama_lengkap":
                                      namaLengkapController.text,
                                      "tanggal_lahir":
                                      tanggalLahirController.text,
                                      "nomor_telepon":
                                      nomorTeleponController.text,
                                    });
                                    Navigator.pop(context);
                                  }
                                  if (nomorTeleponController.text.length < 7) {
                                    setState(() {
                                      _validateMinNomorTelepon = true;
                                    });
                                  }
                                  if (nomorTeleponController.text == "") {
                                    setState(() {
                                      _validateNomorTelepon = true;
                                    });
                                  }
                                  if (namaLengkapController.text == ""){
                                    setState(() {
                                      _validateNamaLengkap = true;
                                    });
                                  }
                                }),
                          ])),
                    ],
                  ),
                )),
          ],
        ));
  }

  void _openGallery(BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    setState(() {
      imageFile = pickedFile;
      image = FileImage(File(imageFile.path));
    });

    Navigator.pop(context, true);
  }

  void _openCamera(BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
    );
    setState(() {
      imageFile = pickedFile;
      image = FileImage(File(imageFile.path));
    });
    Navigator.pop(context);
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Choose option",
              style: TextStyle(color: Colors.blue),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                    },
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                    },
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
