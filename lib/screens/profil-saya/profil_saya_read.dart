import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lapor_premanisme/screens/profil-saya/profil_saya_update.dart';
import 'package:firebase_storage/firebase_storage.dart';

class ProfilSayaRead extends StatefulWidget {
  @override
  ProfilState createState() => ProfilState();
}

class ProfilState extends State<ProfilSayaRead> {
  List userProfileList = [];
  User user = FirebaseAuth.instance.currentUser;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  String jenisKelamin = "";
  String namaLengkap = "";
  String nik = "";
  String tanggalLahir = "";
  String nomorTelepon = "";
  String email = "";
  String url = "https://i.stack.imgur.com/bKTJc.png";
  final TextEditingController namaLengkapController = TextEditingController();
  final TextEditingController nikController = TextEditingController();
  var nomorTeleponController = TextEditingController();
  var tanggalLahirController = TextEditingController();
  var emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _userDetails(user.uid);
  }

  Future<void> _userDetails(userID) async {
    final userDetails = await firestore.collection("profil").doc(userID).get();
    final ref = FirebaseStorage.instance.ref().child('foto-profil/${userID}');
    var url;
    try {
      url = await ref.getDownloadURL();
    }
    catch (e) {
      url = "https://cdn.kibrispdr.org/data/download-logo-user-0.jpg";
    }
    if (this.mounted) {
      setState(() {
        this.url = url;
        namaLengkap = userDetails['nama_lengkap'];
        nomorTelepon = userDetails['nomor_telepon'];
        tanggalLahir = userDetails['tanggal_lahir'];
        nik = userDetails['nik'];
        email = user.email;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Container(
            child: ListView(
          children: [
            SizedBox(
              height: 60,
            ),
            Column(children: [
              Image.network(
                  'https://www.linkpicture.com/q/logo-lp-1.png'),
              SizedBox(height: 20),
              Text(
                'PROFIL',
                style: new TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 22.0,
                  color: Colors.red[800],
                ),
              ),
            ]),
            SizedBox(height: 15,),
            Center(
              child: Stack(children: <Widget>[
                CircleAvatar(
                    radius: 80.0,
                    backgroundImage: NetworkImage(url)),
              ]),
            ),
            Container(
                margin: EdgeInsets.only(
                    left: 30, top: 10, right: 30, bottom: 10),
                child: Wrap(children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 25),
                      TextField(
                        controller: emailController..text = email,
                        enabled: false,
                        decoration: InputDecoration(
                          labelText: "Email",
                          labelStyle: TextStyle(color: Colors.black54),
                          border: new OutlineInputBorder(
                              borderSide:
                              new BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.black54, width: 1.5),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: namaLengkapController..text = namaLengkap,
                        enabled: false,
                        decoration: InputDecoration(
                          labelText: "Nama Lengkap",
                          labelStyle: TextStyle(color: Colors.black54),
                          border: new OutlineInputBorder(
                              borderSide:
                              new BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.black54, width: 1.5),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: nikController..text = nik,
                        enabled: false,
                        keyboardType: TextInputType.number,
                        maxLength: 16,
                        decoration: InputDecoration(
                          labelText: "NIK",
                          labelStyle: TextStyle(color: Colors.black54),
                          border: new OutlineInputBorder(
                              borderSide:
                              new BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.black54, width: 1.5),
                          ),
                          counterStyle: TextStyle(
                            height: double.minPositive,
                          ),
                          counterText: "",
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: nomorTeleponController..text = nomorTelepon,
                        enabled: false,
                        keyboardType: TextInputType.phone,
                        maxLength: 16,
                        decoration: InputDecoration(
                          labelText: "Nomor Telepon",
                          labelStyle: TextStyle(color: Colors.black54),
                          border: new OutlineInputBorder(
                              borderSide:
                              new BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.black54, width: 1.5),
                          ),
                          counterStyle: TextStyle(
                            height: double.minPositive,
                          ),
                          counterText: "",
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: tanggalLahirController..text = tanggalLahir,
                        enabled: false,
                        decoration: InputDecoration(
                          labelText: "Tanggal Lahir",
                          labelStyle: TextStyle(color: Colors.black54),
                          border: new OutlineInputBorder(
                              borderSide:
                              new BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.black54, width: 1.5),
                          ),
                        ),
                      ),
                    ],
                  ),
                ])),
            Container(
              // height: 100,
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 80),
              child: Column(
                children: [
                  RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4)),
                      color: Colors.white,
                      child: Text(
                        'UPDATE',
                        style: GoogleFonts.poppins(
                            color: Colors.red[800], fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => ProfilSayaUpdate(this))).then((value) {
                          setState(() {
                            _userDetails(user.uid);
                          });
                        });
                      }),
                ],
              ),
            ),
          ],
        )),
      ]),
    );
  }
}
