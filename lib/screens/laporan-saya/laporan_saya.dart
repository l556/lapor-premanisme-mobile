import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

import '../../database-manager/laporan_db.dart';
import 'detail_laporan_saya.dart';

class LaporanSaya extends StatefulWidget {
  LaporanSaya();
  _LaporanSayaState createState() => _LaporanSayaState();
}

class _LaporanSayaState extends State<LaporanSaya> {
  List userLaporanList = [];
  User user = FirebaseAuth.instance.currentUser;

  _LaporanSayaState() {
  }

  @override
  void initState() {
    super.initState();
    fetchLaporanList();
  }

  fetchLaporanList() async {
    List allLaporan = await LaporanDB().getLaporanList();
    List results = new List();
    allLaporan.forEach((element) {
      if (element['id'] == user.uid) {
        print("TES MASUKKKK");
           results.add(element);
      }
    });
    if (results == null) {
      print('Unable to retrieve');
    } else {
      if (this.mounted) {
        setState(() {
          userLaporanList = results;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(children: [
          Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
              child:
                  Image.network('https://www.linkpicture.com/q/logo-lp-1.png')),
          Text(
            'Laporan Saya',
            style: TextStyle(
              color: Colors.red[800],
            ),
          ),
        ]),
      ),
      body: Stack(children: [
        Container(
            child: ListView.builder(
                itemCount: userLaporanList.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      title: Text(userLaporanList[index]['judul']),
                      subtitle: Text(userLaporanList[index]['deskripsi']),
                      onTap: () async {
                        var urlFirestorage = 'foto-pendukung-laporan/${user.uid}/${userLaporanList[index]['no_laporan']}';
                        final ref = await FirebaseStorage
                            .instance.ref().child(urlFirestorage);
                        var url = null;
                        try {
                          url = await ref.getDownloadURL();
                        }
                        catch (e) {
                        }
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailLaporan(userLaporanList[index], url)),
                        );
                      },
                    ),
                  );
                })),
      ]),
    );
  }
}
