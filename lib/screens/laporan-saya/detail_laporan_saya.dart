import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lapor_premanisme/screens/track-laporan/track_laporan.dart';
import 'package:path/path.dart';

class DetailLaporan extends StatelessWidget {
  final TextEditingController judulLaporanController = TextEditingController();
  final TextEditingController deskripsiLaporanController =
  TextEditingController();
  final TextEditingController errorTextController = TextEditingController();
  var judulLaporan;
  var deskripsiLaporan;
  var dataSource;
  var dataLaporan;
  var lokasiLaporan = "";

  var url;


  DetailLaporan(dataLaporan, url) {
    this.dataLaporan = dataLaporan;
    this.url = url;
    print(url);
    if (dataLaporan is DocumentReference) {
      getData(dataLaporan);
    } else {
      judulLaporan = dataLaporan['judul'];
      deskripsiLaporan = dataLaporan['deskripsi'];
      dataSource = dataLaporan;
      lokasiLaporan = dataSource['lokasi'];
    }
  }

  Future<dynamic> getData(DocumentReference dataLaporan) async {
    this.dataSource = await dataLaporan.get();
    judulLaporan = dataSource['judul'];
    deskripsiLaporan = dataSource['deskripsi'];
    lokasiLaporan = dataSource['lokasi'];
  }

  String id;

  var image = null;

  PickedFile imageFile = null;

  var namaLengkapController;

  User user = FirebaseAuth.instance.currentUser;

  var bigFont = 16.0;
  var medFont = 14.0;
  var smallFont = 12.0;


  Future uploadFile(BuildContext context) async {
    String fileName = basename(imageFile.path);
    UploadTask uploadTask;

    // Create a Reference to the file
    Reference ref =
    FirebaseStorage.instance.ref().child('foto-pendukung-laporan/${id}');
    // .child('foto-profil/${id}');

    uploadTask = ref.putFile(File(imageFile.path));

    return Future.value(uploadTask);
  }

  var status = "Sudah Selesai";
  var coklat  = 0xff574040;
  var oren    = 0xffFC7E47;
  var kuning  = 0xffFAB400;
  var hijau   = 0xff73BF72;
  var warnaStatus;
  var tanggalPelaporan;

  @override
  Widget build(BuildContext context) {
    tanggalPelaporan = dataSource['tanggal_pelaporan'];
    tanggalPelaporan = tanggalPelaporan.toDate();
    tanggalPelaporan = DateFormat.yMMMd().add_jm().format(tanggalPelaporan);
    warnaStatus = hijau;
    if (dataSource['is_sudah_selesai'] == false) {
      status = "Sedang Diproses";
      warnaStatus = kuning;
      if (dataSource['is_sedang_diproses'] == false) {
        status = "Sedang Diverifikasi";
        warnaStatus = oren;
        if (dataSource['is_sedang_diverifikasi'] == false) {
          status = "Belum Ditindaklanjuti";
          warnaStatus = coklat;
        }
      }
    }

    var title = GoogleFonts.roboto(
      color: Color(0xff574040),
      letterSpacing: 1.5,
      fontSize: 16,
    );

    var subTitle = GoogleFonts.roboto(
      fontSize: medFont,
      color: Color(0xff000000).withOpacity(0.6),
    );

    var descriptionData = GoogleFonts.roboto(
      fontSize: bigFont,
      color: Color(0xff000000),
    );

    var color = 0xff574040;
    var statusStyle = GoogleFonts.roboto(
      fontSize: bigFont,
      color: Color(warnaStatus),
    );

    var dateStatusStyle = GoogleFonts.roboto(
      fontSize: smallFont,
      fontWeight: FontWeight.w600,
    );

    var verticalSpaceForInformasiLaporan = 3.0;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(-5, 0),
                        blurRadius: 15,
                        spreadRadius: 3)
                  ]),
                  width: double.infinity,
                  height: 1000,
                  child: ListView(
                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                    children: [
                      Column(children: [
                        SizedBox(height: 80),
                        Image.network(
                            'https://www.linkpicture.com/q/logo-lp-1.png'),
                        SizedBox(height: 20),
                        Text(
                          'LAPORAN SAYA',
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 22.0,
                            color: Colors.red[800],
                          ),
                        ),
                      ]),
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 120,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              alignment: Alignment.topLeft,
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 20),
                                    Text(
                                      "Informasi Laporan",
                                      style: subTitle,
                                    ),
                                    SizedBox(height: 5),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text("No Laporan: ${dataSource['no_laporan']}", style: descriptionData,),
                                        SizedBox(height: verticalSpaceForInformasiLaporan),
                                        Text("Tgl Pelaporan: ${tanggalPelaporan}",style: descriptionData,),
                                        SizedBox(height: verticalSpaceForInformasiLaporan),
                                        Text.rich(
                                            TextSpan(
                                              text: "Status: ",
                                              style: descriptionData,
                                              children: <InlineSpan>[
                                                TextSpan(
                                                  text: "${status}",
                                                  style: statusStyle
                                                )
                                              ]
                                            )
                                        ),
                                        ButtonTheme(
                                          minWidth: 150,
                                          child: RaisedButton(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(4)),
                                              color: Colors.red[800],
                                              child: Text(
                                                'LACAK STATUS PENANGANAN',
                                                style: GoogleFonts.poppins(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold),
                                              ),
                                              onPressed: () {

                                                Navigator.of(context)
                                                    .push(MaterialPageRoute(builder: (context) => TrackLaporan(dataSource)));
                                              }),
                                        ),
                                      ],
                                    )
                                  ]),
                            ),
                            SizedBox(height: 5),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                              child: Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Judul Laporan",style: subTitle,),
                                      SizedBox(height: 5,),
                                      Text(judulLaporan, style: descriptionData,)]),
                              ),
                            ),
                            // SizedBox(height: 25),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                              child: Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Dekripsi Laporan",style: subTitle,),
                                      SizedBox(height: 5,),
                                      Text(deskripsiLaporan, style: descriptionData,)]),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                              child: Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Foto Pendukung",style: subTitle,),
                                      SizedBox(height: 5,),
                                      Container(
                                        child: Image(
                                          image: url == null? AssetImage("no_image.png") : NetworkImage(url),
                                        ),
                                      ),
                                    ]),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                              child: Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Lokasi",style: subTitle,),
                                      SizedBox(height: 5,),
                                      Text(lokasiLaporan, style: descriptionData,)]),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          height: 300,
                          width: 300,
                          padding: const EdgeInsets.fromLTRB(0, 25, 0, 15),
                          child: Column(children: [
                            RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.white,
                                child: Text(
                                  'KEMBALI',
                                  style: GoogleFonts.poppins(
                                      color: Colors.red[800],
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  Navigator.pop(context, "Back");
                                }),
                          ])),
                    ],
                  ),
                )),
          ],
        ));
  }
}
