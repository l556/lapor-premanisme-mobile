import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lapor_premanisme/screens/profil-saya/profil_saya_read.dart';
import 'package:path_provider/path_provider.dart';
import '../../database-manager/laporan_db.dart';
import 'package:place_picker/place_picker.dart';

class FormLaporan extends StatefulWidget {
  @override
  _FormLaporanState createState() => _FormLaporanState();
}

class _FormLaporanState extends State<FormLaporan> {
  final TextEditingController judulLaporanController = TextEditingController();
  final TextEditingController deskripsiLaporanController =
      TextEditingController();
  final TextEditingController errorTextController = TextEditingController();
  final TextEditingController FotoController = TextEditingController();

  String id;

  var image = null;

  PickedFile imageFile = null;

  var namaLengkapController;

  User user = FirebaseAuth.instance.currentUser;

  Future<void> laporan;

  List userLaporanList;

  var noLaporan;

  String location = "";

  var _validateJudul = false;

  bool _validateDeskripsi = false;

  Future uploadFile(BuildContext context) async {
    String fileName = basename(imageFile.path);
    UploadTask uploadTask;

    // Create a Reference to the file
    Reference ref = FirebaseStorage.instance
        .ref()
        .child('foto-pendukung-laporan/${user.uid}/${noLaporan}');
    // .child('foto-profil/${id}');

    uploadTask = ref.putFile(File(imageFile.path));

    return Future.value(uploadTask);
  }

  Future<File> getImageFileFromAssets(String path) async {
    final byteData = await rootBundle.load('assets/$path');

    final file = File('${(await getTemporaryDirectory()).path}/$path');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return file;
  }

  fetchLaporanList() async {
    List allLaporan = await LaporanDB().getLaporanList();
    List results = new List();
    allLaporan.forEach((element) {
      if (element['id'] == user.uid) {
        results.add(element);
      }
    });
    if (results == null) {
      print('Unable to retrieve');
    } else {
      return results;
    }
  }

  @override
  Widget build(BuildContext context) {
    // image = getImageFileFromAssets("camera_lapor_preman.png");
    void showPlacePicker() async {
      LocationResult result = await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) =>
              PlacePicker("AIzaSyAHoJvEQFN7x2iWWalFOGLS6Lxk0u8Ybts")));

      // Handle the result in your way
      setState(() {
        location = result.formattedAddress;
      });
      print(result.formattedAddress);
    }

    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(-5, 0),
                        blurRadius: 15,
                        spreadRadius: 3)
                  ]),
                  width: double.infinity,
                  height: 1000,
                  child: ListView(
                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                    children: [
                      Column(children: [
                        SizedBox(height: 80),
                        Image.network(
                            'https://www.linkpicture.com/q/logo-lp-1.png'),
                        SizedBox(height: 20),
                        Text(
                          'PELAPORAN',
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 22.0,
                            color: Colors.red[800],
                          ),
                        ),
                      ]),
                      SizedBox(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width - 120,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: 25),
                            TextField(
                              controller: judulLaporanController,
                              decoration: InputDecoration(
                                labelText: "Judul Laporan",
                                labelStyle: TextStyle(color: Colors.black54),
                                border: new OutlineInputBorder(
                                    borderSide:
                                    new BorderSide(color: Colors.black)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.black54, width: 1.5),
                                ),
                                errorText:
                                    _validateJudul ? 'Mohon diisi' : null,
                              ),
                            ),
                            SizedBox(height: 25),
                            TextField(
                              controller: deskripsiLaporanController,
                              decoration: InputDecoration(
                                  errorText:
                                      _validateDeskripsi ? 'Mohon diisi' : null,
                                  labelText: "Deskripsi Laporan",
                                  labelStyle: TextStyle(color: Colors.black54),
                                  border: new OutlineInputBorder(
                                      borderSide:
                                      new BorderSide(color: Colors.black)),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.black54, width: 1.5),
                                  )),
                              keyboardType: TextInputType.multiline,
                              maxLines: 4,
                            ),
                            SizedBox(height: 25),
                            Text("Foto Pendukung",
                                textAlign: TextAlign.left,
                                style: TextStyle(color: Colors.black54)),
                            InkWell(
                              child: Container(
                                child: Image(
                                  // image: image,
                                  image: image == null
                                      ? AssetImage("camera_lapor_preman.png")
                                      : image,
                                ),
                              ),
                              onTap: () => {_showChoiceDialog(context)},
                            ),
                        SizedBox(height: 25),
                          Text("Lokasi",
                              textAlign: TextAlign.left,
                              style: TextStyle(color: Colors.black54)),
                        InkWell(
                          child: Container(
                              child: Column(
                                children: [ElevatedButton(
                                  child: Text("Pilih Lokasi"),
                                  onPressed: () {
                                    showPlacePicker();
                                  },
                                  style: TextButton.styleFrom(backgroundColor: Colors.grey),
                                ), Text(
                                    location
                                )],

                              )
                          ),
                        )
                          ],
                        ),
                      ),

                      Container(
                          height: 300,
                          width: 300,
                          padding: const EdgeInsets.fromLTRB(0, 25, 0, 15),
                          child: Column(children: [
                            RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.red[800],
                                child: Text(
                                  'LAPOR',
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () async {
                                  if (judulLaporanController.text != "" &&
                                      deskripsiLaporanController.text != "") {
                                    List laporanList = await fetchLaporanList();
                                    noLaporan = 1;
                                    if (laporanList != null) {
                                      noLaporan = laporanList.length + 1;
                                    }
                                    if (judulLaporanController.text != "" &&
                                        deskripsiLaporanController.text != "") {
                                      laporan = LaporanDB().createLaporan(
                                        judulLaporanController.text,
                                        deskripsiLaporanController.text,
                                        user.uid,
                                        noLaporan,
                                        location
                                      );
                                      if (image != null) {
                                        await uploadFile(context).timeout(
                                            const Duration(seconds: 10));
                                      }
                                      Navigator.pop(context, laporan);
                                      judulLaporanController.text = "";
                                      deskripsiLaporanController.text = "";
                                    }
                                  }

                                  if (deskripsiLaporanController.text == "") {
                                    setState(() {
                                      _validateDeskripsi = true;
                                    });
                                  }

                                  if (judulLaporanController.text == "") {
                                    setState(() {
                                      _validateJudul = true;
                                    });
                                  }
                                }),
                            RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)),
                                color: Colors.white,
                                child: Text(
                                  'KEMBALI',
                                  style: GoogleFonts.poppins(
                                      color: Colors.red[800],
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                }),
                          ])),
                    ],
                  ),
                )),
          ],
        ));
  }

  void _openGallery(BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    setState(() {
      imageFile = pickedFile;
      image = FileImage(File(imageFile.path));
      // image = Image.network(
      //     'https://www.linkpicture.com/q/logo-lp-1.png');
    });

    Navigator.pop(context, true);
  }

  void _openCamera(BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
    );
    setState(() {
      imageFile = pickedFile;
      image = FileImage(File(imageFile.path));
    });
    Navigator.pop(context);
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Choose option",
              style: TextStyle(color: Colors.blue),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                    },
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                    },
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}

class LaporanImageGabung {
  var _laporan;
  var _image;

  get laporan => _laporan;

  LaporanImageGabung(this._laporan, this._image);

  get image => _image;
}
