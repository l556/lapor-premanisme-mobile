import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  final String header;
  final String count;
  final Color headerColor;
  CardWidget(this.header, this.count, this.headerColor);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
        height: 100,
        width: 150,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Column(
            // mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.zero,
                minVerticalPadding: 0.0,
                title: new Container(
                    decoration: BoxDecoration(
                      color: this.headerColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                    ),
                    margin: EdgeInsets.zero,
                    padding: EdgeInsets.zero,
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                        child: Center(
                          child: Text(
                            this.header,
                            style: new TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 12.0,
                              color: Colors.white,
                            ),
                          ),
                        ))),
                subtitle: new Container(
                  padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                  width: 300,
                  color: Colors.white,
                  child: Center(
                      child: Text(
                    this.count,
                    style: new TextStyle(
                        fontWeight: FontWeight.w800, fontSize: 32.0),
                  )),
                ),
              ),
            ]));
  }
}
