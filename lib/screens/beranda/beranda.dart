import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:lapor_premanisme/main_page.dart';
import '../../database-manager/laporan_db.dart';
import "card_widget.dart";

class Beranda extends StatefulWidget {
  @override
  _BerandaState createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  // Keterangan kode status laporan:
  // 0 = belum ditindaklanjuti
  // 1 = sedang diverifikasi
  // 2 = sedang diproses
  // 3 = sudah selesai

  int countBelumDitindaklanjuti = 0;
  int countSedangDiverifikasi = 0;
  int countSedangDiproses = 0;
  int countSudahSelesai = 0;
  int totalLaporan = 0;

  @override
  void initState() {
    super.initState();
    fetchLaporanList();
  }

  void countStatistics(List laporanList) {
    for (var i = 0; i < laporanList.length; i++) {
      try {
        int statusLaporan = laporanList[i]['status_laporan'];
        if (statusLaporan == 0) {
          this.countBelumDitindaklanjuti++;
        } else if (statusLaporan == 1) {
          this.countSedangDiverifikasi++;
        } else if (statusLaporan == 2) {
          this.countSedangDiproses++;
        } else if (statusLaporan == 3) {
          this.countSudahSelesai++;
        }
        this.totalLaporan++;
        // } on Exception catch(e) {
      } catch (e) {
        continue;
      }
    }
  }

  fetchLaporanList() async {
    List results = await LaporanDB().getLaporanList();

    if (results == null) {
      print('Unable to retrieve');
    } else {
      setState(() {
        countStatistics(results);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            title: Row(children: [
              Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                  child: Image.network(
                      'https://www.linkpicture.com/q/logo-lp-1.png')),
              Text(
                'Lapor Premanisme',
                style: TextStyle(
                  color: Colors.red[800],
                ),
              ),
            ])),
        body: new Container(
            // color: const Color(0xFFFFFF),
            child: ListView(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: new Container(
                      margin: EdgeInsets.only(
                          left: 20, top: 10, right: 20, bottom: 10),
                      height: 150,
                      width: 300,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(
                          // mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                                contentPadding: EdgeInsets.zero,
                                minVerticalPadding: 0.0,
                                title: new Container(
                                    // transform: Matrix4.translationValues(
                                    //     -50.0, 0.0, 0.0),
                                    decoration: BoxDecoration(
                                      color: Colors.red[800],
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10),
                                      ),
                                    ),
                                    margin: EdgeInsets.zero,
                                    padding: EdgeInsets.zero,
                                    child: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 15, 0, 15),
                                        child: Center(
                                          child: Text(
                                            'Total Laporan',
                                            style: new TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 20.0,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ))),
                                subtitle: new Container(
                                  padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                  width: 300,
                                  color: Colors.white,
                                  child: Center(
                                      child: Text(
                                    totalLaporan.toString(),
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 60.0),
                                  )),
                                )),
                          ]))),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CardWidget("Belum Ditindaklanjuti",
                  countBelumDitindaklanjuti.toString(), Colors.brown[800]),
              CardWidget('Sedang Diverifikasi',
                  countSedangDiverifikasi.toString(), Colors.orangeAccent[700]),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CardWidget("Sedang Diproses", countSedangDiproses.toString(),
                  Colors.amber[600]),
              CardWidget('Sudah Selesai', countSudahSelesai.toString(),
                  Colors.greenAccent[700]),
            ],
          ),
        ])));
  }
}
