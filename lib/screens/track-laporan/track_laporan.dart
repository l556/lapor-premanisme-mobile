
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class TrackLaporan extends StatefulWidget {
  var dataSource;

  TrackLaporan(this.dataSource) {

  }

  @override
  _TrackLaporanState createState() {

    return _TrackLaporanState(dataSource);
  }
}

class _TrackLaporanState extends State<TrackLaporan> {
  var bigFont = 16.0;
  var medFont = 14.0;
  var smallFont = 12.0;
  var dataSource;

  var tanggalPelaporan;

  String status = "Sudah Selesai";

  int warnaStatus;

  var coklat = 0xff574040;
  var oren = 0xffFC7E47;
  var kuning = 0xffFAB400;
  var hijau = 0xff73BF72;

  _TrackLaporanState(this.dataSource);

  Column createStatusColumn(judulStatus, deskripsi, tanggal, color) {
    var descriptionData = GoogleFonts.roboto(
      fontSize: bigFont,
      color: Color(0xff000000),
    );

    var statusStyle = GoogleFonts.roboto(
      fontSize: smallFont,
      color: Color(color),
    );
    var dateStatusStyle = GoogleFonts.roboto(
      fontSize: smallFont,
      fontWeight: FontWeight.w600,
    );
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "${judulStatus}",
          style: statusStyle,
        ),
        SizedBox(
          height: 5,
        ),
        Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${deskripsi}",
                style: descriptionData,
              ),
              SizedBox(
                height: 3,
              ),
              Text(
                "${tanggal}",
                style: dateStatusStyle,
              ),
            ])
      ],
    );
  }

  @override
  Widget build(BuildContext context) {

    var title = GoogleFonts.roboto(
      color: Color(0xff574040),
      letterSpacing: 1.5,
      fontSize: 16,
    );

    var subTitle = GoogleFonts.roboto(
      fontSize: medFont,
      color: Color(0xff000000).withOpacity(0.6),
    );

    var descriptionData = GoogleFonts.roboto(
      fontSize: bigFont,
      color: Color(0xff000000),
    );

    warnaStatus = hijau;

    List<Widget> statusList = [];
    tanggalPelaporan = dataSource['tanggal_pelaporan'];
    tanggalPelaporan = tanggalPelaporan.toDate();
    tanggalPelaporan = DateFormat.yMMMd().add_jm().format(tanggalPelaporan);




    if (dataSource['is_sudah_selesai'] == false) {
      status = "Sedang Diproses";
      warnaStatus = kuning;
      if (dataSource['is_sedang_diproses'] == false) {
        status = "Sedang Diverifikasi";
        warnaStatus = oren;
        if (dataSource['is_sedang_diverifikasi'] == false) {
          status = "Belum Ditindaklanjuti";
          warnaStatus = coklat;
        }
      }
    }

    var statusMainStyle = GoogleFonts.roboto(
      fontSize: bigFont,
      color: Color(warnaStatus),
    );

    statusList.add(Text(
      "Status",
      style: subTitle,
    ));
    statusList.add(SizedBox(
      height: 5,
    ));

    if (dataSource['is_sudah_selesai']) {
      statusList.add(createStatusColumn(
          "SUDAH SELESAI",
          dataSource['deskripsi_selesai'],
          DateFormat.yMMMd().add_jm().format(dataSource['tanggal_selesai'].toDate()),
          hijau));
    }

    if (dataSource['is_sedang_diproses']) {
      statusList.add(createStatusColumn(
          "SEDANG DIPROSES",
          dataSource['deskripsi_diproses'],
          DateFormat.yMMMd().add_jm().format(dataSource['tanggal_diproses'].toDate()),
          kuning));
    }

    if (dataSource['is_sedang_diverifikasi']) {
      statusList.add(createStatusColumn(
          "SEDANG DIVERIFIKASI",
          dataSource['deskripsi_verifikasi'],
          DateFormat.yMMMd().add_jm().format(dataSource['tanggal_diverifikasi'].toDate()),
          oren));
    }


    statusList.add(SizedBox(
      height: 5,
    ));

    statusList.add(createStatusColumn("BELUM DITINDAKLANJUTI",
        "Pelaporan Kejadian", tanggalPelaporan, coklat));

    var verticalSpaceForInformasiLaporan = 3.0;

    return Scaffold(
        body: Stack(children: [
      Container(
          child: ListView(
        children: [
          SizedBox(height: 60),
          Column(children: [
            Image.network('https://www.linkpicture.com/q/logo-lp-1.png'),
            SizedBox(height: 20),
            Text(
              'LAPORAN SAYA',
              style: new TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 22.0,
                color: Colors.red[800],
              ),
            ),
          ]),
          Center(
            child: Text(
              "LACAK STATUS PENANGANAN",
              style: title,
            ),
            heightFactor: 2,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Informasi Laporan",
                          style: subTitle,
                        ),
                        SizedBox(height: 5),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "No Laporan: ${dataSource['no_laporan']}",
                              style: descriptionData,
                            ),
                            SizedBox(height: verticalSpaceForInformasiLaporan),
                            Text(
                              "Tgl Pelaporan: ${tanggalPelaporan}",
                              style: descriptionData,
                            ),
                            SizedBox(height: verticalSpaceForInformasiLaporan),
                            Text.rich(TextSpan(
                                text: "Status: ",
                                style: descriptionData,
                                children: <InlineSpan>[
                                  TextSpan(
                                      text: "$status", style: statusMainStyle)
                                ])),
                          ],
                        )
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                  child: Container(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Judul Laporan",
                            style: subTitle,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "${dataSource['judul']}",
                            style: descriptionData,
                          )
                        ]),
                  ),
                ),
                Container(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: statusList
                  ),
                ),
              ],
            ),
          ),
          Center(
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4)),
                  color: Colors.white,
                  child: Text(
                    'KEMBALI',
                    style: GoogleFonts.poppins(
                        color: Colors.red[800], fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  })),
        ],
      )),
    ]));
  }
}
